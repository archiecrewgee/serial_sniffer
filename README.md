# serial_sniffer

Simple sniffer for serial applications on Windows.

# Setup

The setup is as follows (this assumes that Python is installed on the host system):

 1. Clone repository and navigate to directory
 2. Setup virtual environment: `python venv .venv`
 3. Install required libraries: `.\.venv\Scripts\python.exe -m pip install -r requirements.txt`
 4. Run with: `.\.venv\Scripts\python.exe sniffer.py <arguments...>`