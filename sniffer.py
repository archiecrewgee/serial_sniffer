import serial
from time import time
from argparse import ArgumentParser

import multiprocessing
import threading

# custom modules #
from exceptions import SnifferTimeout

# classes #
class SerialSniffer():
    def __init__(self, port: str, baud_rate: int, buffer_size: int = 1024, timeout_s: int = None):
        try:
            self.serial = serial.Serial(port, baud_rate, timeout=timeout_s)
            self.serial.set_buffer_size(rx_size = buffer_size, tx_size = buffer_size)

        except Exception as e:
            raise e

    def sniff(self, queue):
        try:
            while True:
                # read character at a time
                character = self.serial.read(size=1)

                # if timeout has occured then raise exception and exit
                if len(character) == 0:
                    raise SnifferTimeout(self.serial.timeout)
                
                # ignore if not a valid character
                if self.is_char(character):
                    character = str(character, encoding='utf-8')    # convert to utf-8 encoding (now string type)

                    # if a newline character or end of string (0x00) then insert a new line. Otherwise push item to queue
                    if character == '\n' or character == '\0':
                        queue.put("\n")
                    else:
                        queue.put(character)

        except Exception as e:
            raise e

    # confirms if utf-8 chr or not (i.e. fits on the ASCII table)
    def is_char(self, char: bytes) -> bool:
        char = int.from_bytes(char, "big")
        return char < 0x80


if __name__ == "__main__":
    try:
        # get arguments #
        parser = ArgumentParser(description="CLI interface for sniffing serial ports on Windows")
        parser.add_argument("port", help = "Port to read serial data from", type = str)
        parser.add_argument("baud_rate", help = "Baud rate of data received", type = int)
        parser.add_argument("timeout", help = "Timeout (seconds) for serial read", nargs='?', type = int, default=None)

        args = parser.parse_args()

        # setup #
        sniffer = SerialSniffer(args.port, args.baud_rate, timeout_s=args.timeout)

        queue_sniffer = multiprocessing.Queue()
        thread_sniffer = threading.Thread(target=sniffer.sniff, args=(queue_sniffer,), daemon=True)

        # process #
        thread_sniffer.start()

        buffer = ""
        while thread_sniffer.is_alive():
            while queue_sniffer.empty():
                continue
            
            # print chars as they are collected; note that this is treated as a stream and a buffer is constantly rewritten to the stdout
            character = queue_sniffer.get()
            
            print(character, end='')

        sniffer.close()

    except Exception as e:
        sniffer.close()
        raise e