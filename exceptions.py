class SnifferTimeout(Exception):
    def __init__(self, timeout):
        super().__init__(f'Serial sniffer has timed out after {timeout} seconds')
